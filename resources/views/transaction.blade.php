@extends('adminlte::page')

@section('title', 'My transactions')

@section('content_header')
    <h1>My transactions</h1>
@stop

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Transaction Info</h3>
                </div>

                <div class="box-body">
                    <table class="table">
                        <thead>
                            <th>Transaction Id</th>
                            <th>Amount</th>
                            <th>Type</th>
                            <th>Created</th>
                        </thead>

                        <tbody>
                            @foreach($transactions as $transaction)
                                <tr>
                                    <td>{{ $transaction->payment_id }}</td>
                                    <td>${{ $transaction->payment_amount }}</td>
                                    <td><span class="label label-sm label-primary">{{ $transaction->payment_type }}</span></td>
                                    <td>{{ $transaction->created_at->format('M d, Y') }}</td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection