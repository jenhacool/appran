<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Keyapp;
use Auth;

class OrderController extends Controller
{
    public function listOrder(Request $request)
    {
        dd($request->user('api'));
        $user_id = $request->user_id;
        $status = $request->status;

        $app_ids = App::where('user_id', $user_id)->pluck('app_id')->toArray();
        $response = Keyapp::listOrder($status);

        if(isset($response->Error)) {
            return response()->json([
                'error' => $response->Error
            ], 200);
        }

        $orders = $response->orders;

        foreach($orders as $key => $order) {
            if(!in_array((int) $order->idapp, $app_ids)){
                unset($oders[$key]);
            }
        }
    }
}
