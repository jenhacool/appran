<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use Validator;
use DB;
use App\User;
use Hash;
use App\Wallet;

class AjaxController extends Controller
{
    public function login(Request $request)
    {
        $rules = [
            'email' =>'required|email',
            'password' => 'required'
        ];

        $validator = Validator::make($request->all(), $rules);

        if($validator->fails()) {
            return response()->json([
                'error' => true,
                'message' => 'Please check email and password again'
            ], 200);
        }

        $email = $request->input('email');
        $password = $request->input('password');

        $old_user = DB::table('custlogin')->where('email', $email)->where('password', md5($password))->first();

        if($old_user) {
            return response()->json([
                'error' => true,
                'message' => 'Please reset your password'
            ], 200);
        }

        if(!Auth::attempt(['email' => $email, 'password' =>$password])) {
            return response()->json([
                'error' => true,
                'message' => 'Please check email and password again'
            ], 200);
        }

        return response()->json([
            'error' => false
        ], 200);
    }

    public function register(Request $request)
    {
    	$rules = [
            'email' =>'required|email|unique:users',
            'name' => 'required',
            'password' => 'required|confirmed'
        ];

        $validator = Validator::make($request->all(), $rules);

        if($validator->fails()) {
            return response()->json([
                'error' => true,
                'message' => $validator->errors()
            ], 200);
        }

        $email = $request->input('email');
        $name = $request->input('name');
        $password = $request->input('password');

        $user = User::create([
            'name' => $name,
            'email' => $email,
            'password' => Hash::make($password),
            'referred_by' => null
        ]);

        $wallet = Wallet::create([
            'user_id' => $user->id,
            'total' => 0
        ]);

        if(!Auth::attempt(['email' => $email, 'password' => $password])) {
            return response()->json([
                'error' => true,
                'message' => 'Please try again'
            ], 200);
        }

        return response()->json([
            'error' => false
        ], 200);
    }
}
