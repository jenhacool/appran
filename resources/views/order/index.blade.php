@extends('adminlte::page')

@section('title', 'My Orders')

@section('content_header')
    <h1>My Orders</h1>
@stop

@section('content')
    <div class="row" style="margin-bottom: 20px;">
        <div class="col-md-12">
            <a href="{{ route('order.new') }}" class="btn btn-success">Add new order</a>
        </div>
    </div>

    <div class="nav-tabs-custom">
        <ul class="nav nav-tabs">
            <li class="active">
                <a href="#all" data-toggle="tab" aria-expanded="true">All Orders</a>
            </li>
            <li class="">
                <a href="#100" data-toggle="tab" aria-expanded="true">Active</a>
            </li>
            <li class="">
                <a href="#250" data-toggle="tab" aria-expanded="true">Completed</a>
            </li>
            <li class="">
                <a href="#150" data-toggle="tab" aria-expanded="true">Canceled</a>
            </li>
            <li class="">
                <a href="#280" data-toggle="tab" aria-expanded="true">Archived</a>
            </li>
        </ul>
        <div class="tab-content">
            <div class="tab-pane active" id="all"></div>
            <div class="tab-pane" id="100"></div>
            <div class="tab-pane" id="250"></div>
            <div class="tab-pane" id="150"></div>
            <div class="tab-pane" id="280"></div>
            <div class="overlay loading text-center" style="display: none;">
                <i class="fa fa-refresh fa-spin"></i>
            </div>
        </div>
    </div>

    <div class="row-fluid" style="text-align:right;">
        <small class="muted">
            <span class="badge" style="margin-right: 5px">Hint</span>It takes 2-3 days for ranks and installs to update and
            show up in your developer console since your order is completed.<br>
            All COMPLETED and CANCELED orders will be moved to ARCHIVED tab after 14 days since stop.
        </small>
    </div>
@stop
@section('css')
    <link rel="stylesheet" href="{{ asset('css/flags.min.css') }}">
    <style>
        .overlay .fa {
            color: #000;
            font-size: 30px;
        }
    </style>
@endsection

@section('js')
    <script>
        $(document).ready(function() {
            $.ajax({
                url: '{{ route('order.list') }}',
                type: 'GET',
                beforeSend: function() {
                    $('.tab-content .loading').show();
                },
                success: function(response) {
                    $('.tab-content .loading').hide();
                    $('.tab-pane.active').append(response);
                    $('table').dataTable();
                }
            });

            $('.nav-tabs li a').click(function() {
                $('.tab-pane').each(function() {
                    $(this).empty();
                });

                let id = $(this).attr('href').split('#')[1];

                $.ajax({
                    url: '{{ route('order.list') }}?status=' + id,
                    type: 'GET',
                    beforeSend: function() {
                        $('.tab-content .loading').show();
                    },
                    success: function(response) {
                        $('.tab-content .loading').hide();
                        $('#' + id).append(response);
                        $('table').dataTable();
                    }
                });
            });
        });

        function getRankKeyword(id) {
            $.ajax({
                url: '{{ route('keyword.api.rank') }}',
                type: 'POST',
                data: {
                    _token: "{{ csrf_token() }}",
                    id: id,
                },
                beforeSend: function() {
                    $('span[data-id=' + id + '] i').addClass('fa-spin');
                },
                success: function(response) {
                    $('span[data-id=' + id + '] span.rank-number').text(response);
                    $('span[data-id=' + id + '] i').removeClass('fa-spin');
                }
            });
        }
    </script>
@endsection