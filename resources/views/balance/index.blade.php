@extends('adminlte::page')

@section('title', 'Add money to balance')

@section('content_header')
    <h1>Add money to balance</h1>
@stop

@section('content')
    @if(session('error'))
        <div class="alert alert-danger alert-dismissible">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <h4><i class="icon fa fa-ban"></i> Error</h4>
            {{ session('error') }}
        </div>
    @endif

    @if(session('success'))
        <div class="alert alert-success alert-dismissible">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <h4><i class="icon fa fa-check"></i> Success</h4>
            {{ session('success') }}
        </div>
    @endif

    <div class="row">
        <div class="col-md-12">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Add funds by PayPal </h3>
                </div>

                <div class="box-body">
                    <form action="{{ route('balance.add')  }}" method="POST">
                        @csrf
                        <div class="input-group col-md-2" style="width:50%;float:left;">
                            <span class="input-group-addon"><i class="fa fa-dollar"></i></span>
                            <input type="text" class="form-control" id="amount" maxlength="5" name="amount" placeholder="0">
                        </div>
                        <button type="submit" id="add" class="btn btn-success" align="center" style="margin-left: 10px;">
                            <i class="fa fa-paypal" style="margin-right:10px"></i>Add Fund by paypal
                        </button>
                        <br>
                        <span>After clicking the button, you will be redirected to PayPal to complete payment.</span>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection