<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\App;
use Keyapp;
use App\Wallet;
use App\Payment;
use View;

class OrderController extends Controller
{
    public function index(Request $request)
    {
        return view('order.index');
    }

    public function newOrder(Request $request)
    {
        $apps = Keyapp::listApp();

        if(Auth::user()->type != 'admin') {
            $idapps = App::where('user_id', Auth::user()->id)->get()->pluck('app_id')->toArray();

            $apps = array_filter($apps, function($value) use ($idapps) {
                return in_array($value->id, $idapps);
            });
        }
        
        $data = array();
        $data['apps'] = $apps;

        $idapp = $request->query('idapp');
        $country = $request->query('country');
        $idkeyword = $request->query('idkeyword');

        $data['idapp'] = $idapp;
        $data['country'] = $country;
        $data['idkeyword'] = $idkeyword;

        if($idapp && $country) {
            $keywords = Keyapp::getKeywords($request->query('idapp'), $request->query('country'));
        }

        if(isset($keywords) && isset($keywords->keywords)) {
            $data['keywords'] = $keywords->keywords;
        }

        return view('order.new', $data);
    }

    public function listOrder(Request $request)
    {
        $response = ($request->status) ? Keyapp::listOrder($request->status) : Keyapp::listOrder();

        if(Auth::user()->type != 'admin') {
            $app_ids = App::where('user_id', Auth::user()->id)->pluck('app_id')->toArray();
        } else {
            $app_ids = App::all()->pluck('app_id')->toArray();
        }

        if(isset($response->Error)) {
            return response()->json([
                'error' => $response->Error
            ], 200);
        }

        $orders = $response->orders;

        $orders = array_filter($orders, function($value) use ($app_ids) {
            return in_array((int) $value->idapp, $app_ids);
        });

        if(!empty($orders)) {
            foreach($orders as $key => $order) {
                $orders[$key]->app = Keyapp::getApp($order->idapp);
                $orders[$key]->email = App::where('app_id', $order->idapp)->first()->user->email;
            }
        }

        $html = view('order.table')->with('orders', $orders)->render();

        return $html;
    }

    public function storeOrder(Request $request)
    {
        $order = $request->all();

        $wallet = Auth::user()->wallet;

        $total_price = floatval($request->total_price);

        if(floatval($wallet->total) < $total_price) {
            $not_enough_money = 'You do not have enough money. Please add money before creating new order';
            return redirect(route('order.new'))->with('not_enough_money', $not_enough_money);
        }

        $response = Keyapp::addOrder($order);

        if(isset($response->Error)) {
            $error = $response->Error;
            $store_order_error = $error;
            return redirect(route('order.new'))->with('store_order_error', $store_order_error);
        }

        $wallet->total = $wallet->total - $total_price;
        $wallet->save();

        $payment = new Payment();
        $payment->payment_id = (isset($response->id) ? $response->id : '');
        $payment->payment_amount = $total_price;
        $payment->payment_type = 'New Order';
        $payment->user_id = Auth::user()->id;
        $payment->save();

        if(Auth::user()->referred_by) {
            $detail = 'User ' + Auth::user()->email + ' add new order';
            $this->reward(Auth::user()->referred_by, $total_price * 0.2, $detail);
        }

        return redirect(route('order.index'));
    }

    public function reward($user_id, $money, $payment_id)
    {
        $wallet = Wallet::where('user_id', $user_id)->get();
        $wallet->total = $wallet->total + $money;
        $wallet->save();

        $payment = new Payment();
        $payment->payment_id = $payment_id;
        $payment->payment_amount = $money;
        $payment->payment_type = 'Reward';
        $payment->user_id = $user_id;
        $payment->save();
    }

    public function pauseOrder($id)
    {
        $response = Keyapp::pauseOrder($id);

        return redirect()->back();
    }

    public function cancelOrder($id)
    {
        $response = Keyapp::cancelOrder($id);

        return redirect()->back();
    }

    public function resumeOrder($id)
    {
        $response = Keyapp::resumeOrder($id);

        return redirect()->back();
    }

}
