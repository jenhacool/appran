<?php

namespace App\Services;

use PayPal\Rest\ApiContext;
use PayPal\Auth\OAuthTokenCredential;
use PayPal\Api\Item;
use PayPal\Api\ItemList;
use PayPal\Api\Amount;
use PayPal\Api\Transaction;
use PayPal\Api\RedirectUrls;
use PayPal\Api\Payment;
use PayPal\Api\Payer;
use PayPal\Api\PaymentExecution;
use Illuminate\Support\Facades\Input;

class PaypalService {
    private $apiContext;
    private $currency;
    private $returnUrl;
    private $cancelUrl;

    public function __construct()
    {
        $this->apiContext = new ApiContext(
            new OAuthTokenCredential(
                config('paypal.client_id'),
                config('paypal.secret')
            )
        );

        $this->currency = 'USD';
    }

    public function setReturnUrl($url)
    {
        $this->returnUrl = $url;

        return $this;
    }

    public function getReturnUrl()
    {
        return $this->returnUrl;
    }

    public function setCancelUrl($url)
    {
        $this->cancelUrl = $url;

        return $this;
    }

    public function getCancelUrl()
    {
        return $this->cancelUrl;
    }

    public function createPayment($number, $transactionDescription)
    {
        $payer = new Payer();
        $payer->setPaymentMethod('paypal');

        $amount = new Amount();
        $amount->setCurrency($this->currency)->setTotal($number);

        $transaction = new Transaction();
        $transaction->setAmount($amount)->setDescription($transactionDescription);

        $redirectUrls = new RedirectUrls();
        $redirectUrls->setReturnUrl($this->returnUrl)->setCancelUrl($this->cancelUrl);

        $payment = new Payment();
        $payment->setIntent('Sale')->setPayer($payer)->setRedirectUrls($redirectUrls)->setTransactions([$transaction]);

        try {
            $payment->create($this->apiContext);
        } catch (PayPal\Exception\PayPalConnectionException $paypalException) {
            throw new \Exception($paypalException->getMessage());
        }

        $checkoutUrl = $payment->getApprovalLink();
        session(['paypal_payment_id' => $payment->getId()]);

        return $checkoutUrl;
    }

    public function getPaymentStatus()
    {
        $paymentId = session('paypal_payment_id');
//        session()->forget('paypal_payment_id');

        if(empty(Input::get('PayerID')) || empty(Input::get('token'))) {
            return false;
        }

        $payment = Payment::get($paymentId, $this->apiContext);

        $execution = new PaymentExecution();
        $execution->setPayerId(Input::get('PayerID'));

        $status = $payment->execute($execution, $this->apiContext);

        return $status;
    }

    public function getPaymentDetail($paymentId)
    {
        try {
            $paymentDetail = Payment::get($paymentId, $this->apiContext);
        } catch (\PayPal\Exception\PPConnectionException $paypalException) {
            throw new \Exception($paypalException->getMessage());
        }

        return $paymentDetail;
    }
}