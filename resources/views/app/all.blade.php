@extends('adminlte::page')

@section('title', 'My Apps')

@section('content_header')
    <h1>My Apps</h1>
@stop

@section('content')
    <div class="row" style="margin-bottom: 20px;">
        <div class="col-md-12">
            <p>Add your awesome app to promote and keep an eye</p>
            <button type="button" class="btn btn-success" data-toggle="modal" data-target="#new-app">
                Add new app to your list
            </button>
        </div>
    </div>

    @if(session('error'))
        <div class="alert alert-danger alert-dismissible">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <h4><i class="icon fa fa-ban"></i> Error</h4>
            {{ session('error') }}
        </div>
    @endif

    <div class="row">
        @foreach($apps as $app)
            <div class="col-md-6 col-sm-12">
                <div class="box box-solid app-detail">
                    <div class="box-header with-border">
                        <h4 class="box-title">{{ $app->title }}</h4>
                        <button type="button" class="close" data-title="{{ $app->title }}" data-id="{{ $app->id }}" data-toggle="modal" data-target="#delete-app">×</button>
                    </div>
                    <div class="box-body">
                        <div class="row">
                            <div class="col-md-2">
                                <img src="{{ $app->icon }}" alt="">
                            </div>

                            <div class="col-md-10">
                                <span>Publisher</span>
                                <span><a href="https://play.google.com/store/apps/developer?id={{ $app->developer }}">{{ $app->developer }}</a></span>
                                <span>Download</span>
                                <span><strong>{{ $app->downloads }}</strong></span>
                            </div>
                        </div>
                    </div>
                    <div class="box-footer">
                        <a href="{{ route('order.new') . '?idapp=' . $app->id . '&country=' . $app->country }}" class="btn btn-success btn-sm"><i class="fa fa-signal" style="margin-right: 5px;"></i>Promote</a>
                    </div>
                </div>
            </div>
        @endforeach
    </div>

    <div class="modal" id="new-app">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">
                        <strong>Add new application</strong>
                    </h4>
                </div>
                <form action="{{ route('app.store') }}" method="POST">
                    @csrf
                    <div class="modal-body">
                            <div class="form-group">
                                <label for="">Insert url to your app on google play or package name</label>
                                <input type="text" name="url" class="form-control url" placeholder="For example rissotto.sms_box_loved or https://play.google.com/store/apps/details?id=rissotto.sms_box_loved">
                            </div>

                            <div class="form-group">
                                <label for="">Default country of application:</label>
                                <select class="form-control country" name="country">
                                    <option value="US" selected>United States</option>
                                    <option value="GB">Great Britain</option>
                                    <option value="DE">Germany</option>
                                    <option value="IN">India</option>
                                    <option value="CA">Canada</option>
                                </select>
                            </div>

                    </div>

                    <div class="modal-footer">
                        <button class="btn btn-success" type="submit">Add</button>
                        <button class="btn" data-dismiss="modal">Cancel</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <div class="modal" id="delete-app">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">
                        <strong>Delete application</strong>
                    </h4>
                </div>

                <div class="modal-body">
                    <p>Are you sure you want delete <span class="app-name"></span> app?</p>
                </div>

                <form action="" method="POST">
                    @csrf
                    <div class="modal-footer">
                        <button class="btn btn-success" type="submit">Confirm</button>
                        <button class="btn" data-dismiss="modal">Cancel</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@stop
@section('css')
<style>
    .app-detail img {
        width: 100%;
    }
    .app-detail span {
        display:block;
    }

    .app-detail span a {
        font-weight: bold;
    }

    button.close {
        font-size: 26px;
        line-height: 18px;
    }
</style>
@endsection

@section('js')
<script>
    $(document).ready(function() {
        $('button.close').click(function() {
            let name = $(this).attr('data-title');
            let id = $(this).attr('data-id');
            $('#delete-app form').attr('action', '{{ route("app.list") }}' + '/delete/' + id);
            $('#delete-app span.app-name').text(name);
        });
    });
</script>
@endsection