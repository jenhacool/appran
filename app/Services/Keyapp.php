<?php

namespace App\Services;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;

class Keyapp
{
    protected $endPoint = 'https://api.keyapp.top';
    protected $apiKey;
    protected $client;

    public function __construct()
    {
        $this->apiKey = env('API_KEY');
        $this->client = new Client([
            'base_uri' => $this->endPoint,
            'headers' => [
                'Authorization' => 'APIKEY ' . $this->apiKey,
                'Content-type' => 'application/json'
            ]
        ]);
    }

    public function listApp()
    {
        $url = 'apps/list';
        $data = $this->ajaxGet($url);
        $apps = $data->apps;

        return $apps;
    }

    public function getApp($id)
    {
        $url = 'apps/get?id=' . $id;
        $data = $this->ajaxGet($url);

        return $data;
    }

    public function storeApp(array $app)
    {
        try {
            $response = $this->client->request('POST', 'apps/add', [
                'form_params' => [
                    'package' => $app['package'],
                    'country' => $app['country']
                ]
            ]);
        } catch(ClientException $e) {
            $error = $e->getResponse()->getBody()->getContents();
            return $error;
        }

        return $response->getBody();
    }

    public function deleteApp($id)
    {
        try {
            $response = $this->client->request('POST', 'apps/delete', [
                'form_params' => [
                    'id' => $id,
                ]
            ]);
        } catch(ClientException $e) {
            $error = $e->getResponse()->getBody()->getContents();
            return $error;
        }

        return $response->getBody();
    }

    public function getKeywords($idapp, $country)
    {
        $url = 'keywords/list?idapp=' . $idapp . '&country=' . $country;
        $data = $this->ajaxGet($url);

        return $data;
    }

    public function saveKeywords($idapp, $country, array $keywords)
    {
        $url = 'keywords/add';
        $params = [
            'idapp' => $idapp,
            'country' => $country,
            'keywords' => $keywords
        ];

        $data = $this->ajaxPost($url, $params);

        return $data;
    }

    public function getRankKeyword($id)
    {
        $url = 'keywords/rank';
        $params = [
            'id' => $id,
        ];

        $data = $this->ajaxPost($url, $params);

        return $data;
    }

    public function deleteKeyword($id)
    {
        $url = 'keywords/delete';
        $params = [
            'id' => $id,
        ];

        $data = $this->ajaxPost($url, $params);

        return $data;
    }

    public function listOrder($status = null)
    {
        if($status) {
            $url = 'orders/list?status=' . $status;
        } else {
            $url = 'orders/list';
        }

        $data = $this->ajaxGet($url);

        return $data;
    }

    public function addOrder($order)
    {
        $url = 'orders/add';
        $params = $order;

        $data = $this->ajaxPost($url, $params);

        return $data;
    }

    public function cancelOrder($id)
    {
        $url = 'orders/cancel';
        $params = [
            'id' => $id
        ];

        $data = $this->ajaxPost($url, $params);

        return $data;
    }

    public function pauseOrder($id)
    {
        $url = 'orders/pause';
        $params = [
            'id' => $id
        ];

        $data = $this->ajaxPost($url, $params);

        return $data;
    }

    public function resumeOrder($id)
    {
        $url = 'orders/resume';
        $params = [
            'id' => $id
        ];

        $data = $this->ajaxPost($url, $params);

        return $data;
    }

    public function ajaxGet($url) {
        try {
            $response = $this->client->request('GET', $url);
        } catch(ClientException $e) {
            $error = $e->getResponse()->getBody();
            return json_decode($error);
        }

        return json_decode($response->getBody());
    }

    public function ajaxPost($url, array $params) {
        try {
            $response = $this->client->request('POST', $url, [
                'form_params' => $params
            ]);
        } catch(ClientException $e) {
            $error = $e->getResponse()->getBody();
            return json_decode($error);
        }

        return json_decode($response->getBody());
    }
}