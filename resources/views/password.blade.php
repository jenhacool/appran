@extends('adminlte::page')

@section('title', 'Change password')

@section('content_header')
    <h1>Change password</h1>
@stop

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="box">
                <div class="box-header with-border">
                    <h3 class="box-title">Change password</h3>
                </div>

                <form action="{{ route('user.password') }}" method="POST">
                    @csrf
                    <div class="box-body">

                        @if (session('error'))
                            <div class="alert alert-danger">
                                {{ session('error') }}
                            </div>
                        @endif

                        @if (session('success'))
                            <div class="alert alert-success">
                                {{ session('success') }}
                            </div>
                        @endif

                        <div class="form-group">
                            <label for="">Current Password</label>
                            <input type="password" name="current-password" class="form-control">
                            @if ($errors->has('current-password'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('current-password') }}</strong>
                                </span>
                            @endif
                        </div>

                        <div class="form-group">
                            <label for="">New Password</label>
                            <input type="password" name="new-password" class="form-control">

                            @if ($errors->has('new-password'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('new-password') }}</strong>
                                </span>
                            @endif
                        </div>

                        <div class="form-group">
                            <label for="">Confirm New Password</label>
                            <input type="password" name="new-password_confirmation" class="form-control">
                        </div>
                    </div>

                    <div class="box-footer">
                        <button class="btn btn-primary" type="Submit">Change Password</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection