<?php

use Illuminate\Database\Seeder;
use App\Services\Keyapp;
use App\App;

class AppTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $keyapp = new Keyapp;
        $apps = $keyapp->listApp();

        foreach($apps as $app) {
            App::create([
                'app_id' => $app->id,
                'user_id' => 1,
                'package' => $app->package,
                'developer' => $app->developer,
                'icon' => $app->icon,
                'downloads' => $app->downloads,
                'alive' => true,
                'country' => $app->country
            ]);
        }
    }
}
