<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::prefix('keywords')->group(function() {
    Route::post('/', 'Api\KeywordController@saveKeyword')->name('keyword.api.save');
    Route::get('list', 'Api\KeywordController@listKeyword')->name('keyword.api.list');
    Route::post('/rank', 'Api\KeywordController@getRankKeyword')->name('keyword.api.rank');
    Route::post('/delete', 'Api\KeywordController@deleteKeyword')->name('keyword.api.delete');
});

Route::prefix('orders')->middleware('auth:api')->group(function() {
    Route::get('list', 'Api\OrderController@listOrder')->name('order.api.list');
});
