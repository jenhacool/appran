<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Auth;
use App\Payment;
use App\Contact;
use Hash;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('home');
    }

    public function showAffiliatePage()
    {
        $referred = User::where('referred_by', Auth::user()->id)->count();
        $transactions = Payment::where('payment_type', 'Reward')->where('user_id', Auth::user()->id)->get();

        return view('affiliate', compact('referred', 'transactions'));
    }

    public function showContactForm()
    {
        return view('contact');
    }

    public function storeContact(Request $request)
    {
        Contact::create([
           'email' => $request->email,
           'message' => $request->message,
           'status' => 0
        ]);

        return redirect()->back()->with('success', 'We will get back to you as soon as possible');
    }

    public function showContactList()
    {
        $contacts = Contact::all();

        return view('contact-list', compact('contacts'));
    }

    public function markContact($id)
    {
        $contact = Contact::find($id);
        $contact->status = 1;
        $contact->save();

        return redirect()->back();
    }

    public function deleteContact()
    {
        $contact = Contact::find($id);
        $contact->delete();

        return redirect()->back();
    }

    public function changeEmailForm()
    {
        return view('email');
    }

    public function changeEmailUser(Request $request)
    {
        $validate = $request->validate([
            'email' => 'required|email'
        ]);

        $user = Auth::user();
        $user->email = $request->email;
        $user->save();
        return redirect()->back()->with("success","Email changed successfully !");
    }

    public function showUserEditForm()
    {
        return view('password');
    }

    public function changeUserPassword(Request $request)
    {
        if (!(Hash::check($request->get('current-password'), Auth::user()->password))) {
            // The passwords matches
            return redirect()->back()->with("error","Your current password does not matches with the password you provided. Please try again.");
        }

        if(strcmp($request->get('current-password'), $request->get('new-password')) == 0){
            //Current password and new password are same
            return redirect()->back()->with("error","New Password cannot be same as your current password. Please choose a different password.");
        }

        $validatedData = $request->validate([
            'current-password' => 'required',
            'new-password' => 'required|string|min:6|confirmed',
        ]);

        //Change Password
        $user = Auth::user();
        $user->password = bcrypt($request->get('new-password'));
        $user->save();
        return redirect()->back()->with("success","Password changed successfully !");
    }
}
