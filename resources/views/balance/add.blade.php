@extends('adminlte::page')

@section('title', 'Add money to balance')

@section('content_header')
    <h1>Add money to balance</h1>
@stop

@section('content')
    <div class="row">
        <div class="col-md-4">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Add money to user's wallet</h3>
                </div>

                <div class="box-body">
                    <form action="{{ route('balance.transfer')  }}" method="POST">
                        @csrf
                        <div class="form-group">
                            <label for="">Amount</label>
                            <input type="text" class="form-control" id="amount" maxlength="5" name="amount" placeholder="0">
                        </div>
                        <div class="form-group">
                            <label for="">User</label>
                            <select name="user" id="" class="form-control">
                                @foreach($users as $user)
                                    <option value="{{ $user->id }}">{{ $user->email }}</option>
                                @endforeach
                            </select>
                        </div>

                        <div class="form-group">
                            <button type="submit" id="add" class="btn btn-success" align="center">
                                Add
                            </button>
                        </div>
                    </form>
                </div>

                <div class="box-footer">
                    @if(session('error'))
                        <div class="alert alert-danger alert-dismissible">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                            <h4><i class="icon fa fa-ban"></i> Error</h4>
                            {{ session('error') }}
                        </div>
                    @endif

                    @if(session('success'))
                        <div class="alert alert-success alert-dismissible">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                            <h4><i class="icon fa fa-check"></i> Success</h4>
                            {{ session('success') }}
                        </div>
                    @endif
                </div>
            </div>
        </div>
    </div>
@endsection