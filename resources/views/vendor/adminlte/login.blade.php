@extends('adminlte::master')

@section('adminlte_css')
    <link rel="stylesheet" href="{{ asset('css/login.css') }}">
@stop

@section('body')
<div id="fback">
    <div class="girisback"></div>
    <div class="kayitback"></div>
</div>
<div id="textbox">
    <div class="toplam">
        <div class="right">
            <div id="ic">
                <img src="{{ asset('images/logo.png') }}" style="width: 225px;">
                <h2>Sign Up</h2>
                <form class="girisyap" action="{{ url(config('adminlte.register_url', 'register')) }}" method="post" id="register-form">
                    {!! csrf_field() !!}
                    <div class="form-group {{ $errors->has('name') ? 'has-error' : '' }}">
                        <label class="control-label" for="inputNormal">Name</label> 
                        <input class="bp-suggestions form-control" name="name" type="text" value="{{ old('name') }}" placeholder="{{ trans('adminlte::adminlte.full_name') }}">
                    </div>

                    <div class="form-group {{ $errors->has('email') ? 'has-error' : '' }}">
                        <label class="control-label" for="inputNormal">Email</label> 
                        <input class="bp-suggestions form-control" name="email" type="email" value="{{ old('email') }}" placeholder="{{ trans('adminlte::adminlte.email') }}">
                    </div>

                    <div class="form-group {{ $errors->has('password') ? 'has-error' : '' }}">
                        <label class="control-label" for="inputNormal">Password</label> 
                        <input class="bp-suggestions form-control" name="password" type="password" placeholder="{{ trans('adminlte::adminlte.password') }}">
                    </div>

                    <div class="form-group soninpt {{ $errors->has('password_confirmation') ? 'has-error' : '' }}">
                        <label class="control-label" for="inputNormal">Re-Type Password</label>
                        <input class="bp-suggestions form-control" name="password_confirmation" type="password" placeholder="{{ trans('adminlte::adminlte.retype_password') }}">
                    </div>

                    <div class="form-group">
                        <div class="checkbox-list">
                            <div class="checker disabled">
                                <label>
                                    <span class="checked">
                                        <input required class="checked" name="term" id="myCheck" style="float:left;" type="checkbox">I agree with <a href="../terms.html" target="_blank">Term of Service</a> &amp; <a href="../ppolicy.html" target="_blank">Privacy Policy</a></span>
                                </label>
                            </div>
                        </div>
                    </div>
                    <input class="girisbtn" id="signup_submit" name="signup_submit" type="submit" value="Sign Up To Buy Installs , Reviews & Ratings">
                </form>
                <button id="moveleft">Login</button>
            </div>
        </div>
        <div class="left">
            <div id="ic">
                <img src="{{ asset('images/logo.png') }}">
                <h2>Login</h2>
                <form id="login-form" class="girisyap" action="{{ url(config('adminlte.login_url', 'login')) }}" method="post">
                    @csrf
                    <div class="form-group {{ $errors->has('email') ? 'has-error' : '' }}">
                        <label class="control-label">Email</label> 
                        <input class="bp-suggestions form-control" name="email" type="email">
                    </div>
                    <div class="form-group soninpt {{ $errors->has('password') ? 'has-error' : '' }}">
                        <label class="control-label" for="inputNormal">Password</label> 
                        <input class="bp-suggestions form-control" name="password" type="password">
                    </div>
                    <input class="girisbtn" tabindex="100" type="submit" value="Login"> 
                    <a href="#" onclick="forgetpassword();" style="float: left;font-size: 15px;color: tomato;">Forget Password</a>
                </form>
                <form id="forgetPass" class="girisyap" action="{{ url(config('adminlte.password_email_url', 'password/email')) }}" method="post" style="display: none">
                    {!! csrf_field() !!}
                    <div class="form-group">
                        <label class="control-label" for="inputNormal">Email</label> 
                        <input class="bp-suggestions form-control" name="email" type="email"> <input name="action" type="hidden" value="password">
                        <p></p>
                    </div>
                    <input class="girisbtn" tabindex="100" type="submit" value="Reset Password">
                </form>
                <button id="moveright">Sign Up</button>
            </div>
        </div>
    </div>
</div>
@endsection

@section('adminlte_js')
    <script src="{{ asset('js/jquery.validate.min.js') }}"></script>
    <script>
        $('.form-control').on('focus blur', function (e) {
            $(this).parents('.form-group').toggleClass('focused', (e.type === 'focus' || this.value.length > 0));
        }).trigger('blur');

        $('#moveleft').click(function() {
          $('#textbox').animate({
            'marginLeft': "0" //moves left
          });

          $('.toplam').animate({
            'marginLeft': "100%" //moves right
          });
        });

        $('#moveright').click(function() {
          $('#textbox').animate({
            'marginLeft': "50%" //moves right
          });

          $('.toplam').animate({
            'marginLeft': "0" //moves right
          });
        });

        function forgetpassword() {
            $("#login-form").hide();
            $("#forgetPass").show();
            $("#moveright").hide();
        }

        $('#login-form').submit(function(e) {
            e.preventDefault();
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                type: 'POST',
                url: '{{ route("ajax.login") }}',
                data: $(this).serialize(),
                beforeSend: function() {
                    $('.left #ic .ajax-error').remove();
                },
                success: function(response) {
                    if(response.error) {
                        $('.left #ic').append('<div class="ajax-error">' + response.message + '</div>')
                    } else {
                        window.location.href = '{{ route("home") }}';
                    }
                }
            });
        });

        $('#register-form').submit(function(e) {
            e.preventDefault();
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                type: 'POST',
                url: '{{ route("ajax.register") }}',
                data: $(this).serialize(),
                beforeSend: function() {
                    $('.right #ic .ajax-error').remove();
                },
                success: function(response) {
                    if(response.error) {
                        if(typeof response.message === 'string') {
                            $('.right #ic').append('<div class="ajax-error">' + response.message + '</div>');
                        } else {
                            let message = response.message;
                            for(var key in message) {
                                if (message.hasOwnProperty(key)) {
                                    $('.right #ic').append('<div class="ajax-error">' + message[key][0] + '</div>');
                                }
                            }
                        }
                    } else {
                        window.location.href = '{{ route("home") }}';
                    }
                }
            });
        });
    </script>
@stop
