<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Services\PaypalService;
use Auth;
use Illuminate\Support\Facades\Input;
use App\Payment;
use App\Wallet;
use App\User;

class PaymentController extends Controller
{
    private $paypal;

    public function __construct(PaypalService $paypal)
    {
        $this->paypal = $paypal;
    }

    public function index(Request $request)
    {
        if(Auth::user()->type == 'admin') {
            $users = User::all();
            return view('balance.add', compact('users'));
        }

        return view('balance.index');
    }

    public function addBalance(Request $request)
    {
        $amount = $request->amount;
        $user_id = Auth::user()->id;
        $transactionDescription = 'Add money to balance $' . $amount . ' ID ' . $user_id;

        $checkoutUrl = $this->paypal->setReturnUrl(route('balance.callback'))->setCancelUrl(route('balance.index'))->createPayment((int) $amount, $transactionDescription);

        if(!$checkoutUrl) {
            session()->put('error', 'Some error occur, sorry for inconvenient');
            return redirect(route('balance.index'));
        }

        return redirect($checkoutUrl);
    }

    public function transferMoney(Request $request)
    {
        if(Auth::user()->type != 'admin') {
            return redirect(route('home'));
        }

        $amount = $request->amount;
        $user_id = $request->user;

        $wallet = Wallet::where('user_id', $user_id)->first();
        $wallet->total = $wallet->total + $amount;
        $wallet->save();

        $payment = Payment::create([
            'payment_id' => '',
            'payment_amount' => $amount,
            'payment_type' => 'Add money for user ' . User::find($user_id)->email,
            'user_id' => Auth::user()->id
        ]);

        return redirect()->back()->with('success', 'Added money to user balance');
    }

    public function callbackBalance(Request $request)
    {
        $status = $this->paypal->getPaymentStatus();

        if(!$status) {
            session()->put('error', 'Some error occur, sorry for inconvenient');
            return redirect(route('balance.index'));
        }

        $result = $status->getState();

        if($result != 'approved') {
            session()->put('error', 'Payment failed');
            return redirect(route('balance.index'));
        }

        $transaction = $status->getTransactions();
        $relatedResources = $transaction[0]->getRelatedResources();
        $sale = $relatedResources[0]->getSale();
        $saleId = $sale->getId();

        $amount = $sale->getAmount();
        $total = $amount->getTotal();

        $payment = new Payment();
        $payment->payment_id = $saleId;
        $payment->payment_amount = $total;
        $payment->payment_type = 'Add Money To Wallet';
        $payment->user_id = Auth::user()->id;
        $payment->save();

        $this->updateWallet($total);

        session()->put('success', 'Payment success');
        return redirect(route('balance.index'));
    }

    public function updateWallet($total)
    {
        $wallet = Auth::user()->wallet;

        $wallet->total = $wallet->total + $total;
        $wallet->save();
    }

    public function showTransaction()
    {
        if(Auth::user()->type == 'admin') {
            $transactions = Payment::orderBy('created_at', 'desc')->get();
        } else {
            $transactions = Auth::user()->transactions;
        }

        return view('transaction', compact('transactions'));
    }
}
