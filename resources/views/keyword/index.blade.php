@extends('adminlte::page')

@section('title', 'My Keywords')

@section('content_header')
    <h1>My Keywords</h1>
@stop

@section('content')
    <div class="row" style="margin-bottom: 20px;">
        <div class="col-md-8">
            <form action="" class="form-horizontal">
                <div class="form-group row">
                    <label for="" class="col-sm-4 control-label">Country*</label>
                    <div class="col-md-8">
                        <select class="form-control country" name="country">
                            <option value="US" @if(isset($country) && $country == 'US') selected @endif default>United States</option>
                            <option value="GB" @if(isset($country) && $country == 'GB') selected @endif>Great Britain</option>
                            <option value="DE" @if(isset($country) && $country == 'DE') selected @endif>Germany</option>
                            <option value="IN" @if(isset($country) && $country == 'IN') selected @endif>India</option>
                            <option value="CA" @if(isset($country) && $country == 'CA') selected @endif>Canada</option>
                        </select>
                    </div>
                </div>

                <div class="form-group row">
                    <label for="" class="col-sm-4 control-label">Pick your application*</label>
                    <div class="col-md-8">
                        <select class="form-control app" name="country">
                            @if(!isset($idapp))
                                <option value="" selected disabled="">Select App</option>
                            @endif
                            @foreach($apps as $app)
                                <option data-icon="{{ $app->icon }}" value="{{ $app->id }}" @if(isset($idapp) && $idapp == $app->id) selected="selected" @endif>{{ $app->title }}</option>
                            @endforeach
                        </select>
                        <small>If you have no apps you can add new app <a href="{{ route('app.list') }}">here</a></small>
                    </div>
                </div>

                <div class="form-group row">
                    <div class="col-md-12">
                        <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#new-keyword" id="add-keyword" @if(!isset($idapp))style="display: none;"@endif>
                            Add my keywords
                        </button>
                    </div>
                </div>
            </form>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <div class="box box-solid">
                <div class="overlay loading" style="display: none">
                    <i class="fa fa-refresh fa-spin"></i>
                </div>
                <div class="box-body">
                    <div class="text-right"><button class="btn btn-primary all-rank">Update all ranks</button></div>
                    <table class="table keyword-list table-bordered">
                        <thead>
                            <th>Keyword</th>
                            <th>Country</th>
                            <th>Rank</th>
                            <th>Chart</th>
                            <th>Action</th>
                        </thead>
                        <tbody>
                            @if(isset($keywords) && count($keywords) > 0)
                                @foreach($keywords as $keyword)
                                    <tr data-id="{{ $keyword->id }}">
                                        <td>{{ $keyword->keyword }}</td>
                                        <td>{!! $keyword->lang !!}</td>
                                        <td><span class="check-rank" id="{{ $keyword->id }}"><i class="fa fa-refresh"></i> <span class="rank-number">{{ $keyword->rank }}</span></span></td>
                                        <td>Chart</td>
                                        <td>
                                            <button class="btn btn-success btn-sm promote" data-id="{{ $keyword->id }}"><i class="fa fa-signal"></i> Promote</button>
                                            <button class="btn btn-danger btn-sm delete" data-id="{{ $keyword->id }}"><i class="fa fa-trash"></i> Delete</button>
                                        </td>
                                    </tr>
                                @endforeach
                            @else
                                @if(isset($error))
                                    <tr>
                                        <td>{{ $error }}</td>
                                    </tr>
                                @endif
                            @endif
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

    <div class="modal" id="new-keyword">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">
                        <strong>Add keywords</strong>
                    </h4>
                </div>

                <div class="modal-body">
                    <label for="">Please add keyword(s) for promotion:</label>
                    <input type="text" name="keywords[]" data-role="tagsinput" class="form-control keywords-input">
                    <small>For example, <b>spider</b>, <b>chess game</b> or <b>awesome app</b>, add keywords one by one by pressing Enter</small>
                    <div class="text-red" style="margin: 10px 0;">
                        Please don't add any quotes, slashes, hashtags or special symbols to your keywords.
                    </div>
                </div>

                <div class="modal-footer">
                    <button class="btn btn-success add-keyword">Add</button>
                    <button class="btn" data-dismiss="modal">Cancel</button>
                </div>
            </div>
        </div>
    </div>
@stop
@section('css')
    <link rel="stylesheet" href="{{ asset('css/select2.min.css') }}">
    <link rel="stylesheet" href="{{ asset('css/bootstrap-tagsinput.css') }}">
    <link rel="stylesheet" href="{{ asset('css/flags.min.css') }}">
    <style>
        .app-select-option {
            font-size: 18px;
            line-height: 30px;
            display: block;
        }

        .app-select-option img {
            height: 30px;
            margin-right: 10px;
            float: left;
        }

        .select2-selection {
            height: 34px!important;
        }

        .bootstrap-tagsinput {
            display: block;
            font-size: 14px;
        }

        .bootstrap-tagsinput .label {
            line-height: 24px;
            font-size: 100%;
        }
        
        span.check-rank {
            cursor: pointer;
            font-weight: bold;
        }
    </style>
@endsection

@section('js')
    <script src="{{ asset('js/select2.min.js') }}"></script>
    <script src="{{ asset('js/bootstrap-tagsinput.js') }}"></script>
    <script>
        const new_order_url = '{{ route('order.new') }}';

        $(document).ready(function() {
            $('.app').select2({
                templateResult: format
            });

            $('form.form-horizontal select').change(function() {
                if($('.app').val() != '') {
                    $('button#add-keyword').show();
                } else {
                    $('button#add-keyword').hide();
                }

                getKeyword();
            });

            $('button.add-keyword').click(function() {
                let country = $('.country').val();
                let idapp = $('.app').val();
                let keywords = $('.keywords-input').val();
                $('#new-keyword .alert').remove();
                saveKeyword(idapp, country, keywords);
            });

            $('.check-rank').click(function() {
                let id = $(this).attr('id');
                getRankKeyword(id);
            });

            $('.all-rank').click(function() {
                $('table tr span.check-rank').each(function() {
                    let id = $(this).attr('id');
                    getRankKeyword(id);
                });
            });

            $('button.delete').click(function() {
                let id = $(this).attr('data-id');
                deleteKeyword(id);
            });

            $('button.promote').click(function() {
                let idkeyword = $(this).attr('data-id');
                promoteKeyword(idkeyword);
            });
        });

        function loading() {
            $('.loading').show();
        }

        function loaded() {
            $('.loading').hide();
        }

        function format(option) {
            if(!option.id) {
                return option.text;
            }
            var object = $('<div class="app-select-option"><img src="' + $(option.element).attr('data-icon') + '">' + option.text + '</div>');
            return object;
        }

        function getKeyword() {
            let country = $('.country').val();
            let app = $('.app').val();

            if(app != '') {
                loading();
                ajaxRequest(country, app);
            }
        }

        function ajaxRequest(country, app) {
            $.ajax({
                url: '{{ route('keyword.api.list') }}' + '?idapp=' + app + '&country=' + country,
                type: 'GET',
                success: function(response) {
                    showKeyword(response);
                    history.pushState({idapp: app, country: country}, '', '{{ route('keyword.index') }}' + '?idapp=' + app + '&country=' + country);
                    loaded();
                }
            });
        }

        function saveKeyword(idapp, country, keywords) {
            $.ajax({
                url: '{{ route('keyword.api.save') }}',
                type: 'POST',
                data: {
                    _token: "{{ csrf_token() }}",
                    idapp: idapp,
                    country: country,
                    keywords: keywords
                },
                beforeSend: function() {
                    $('button.add-keyword').text('Adding').attr('disabled', 'disabled');
                },
                success: function(response) {
                    if (response.hasOwnProperty('error')) {
                        $('.modal-body').append('<div class="alert alert-danger alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>Keyword(s) already exists</div>')
                    } else {
                        clearAndCloseModal();
                        getKeyword();
                    }
                    $('button.add-keyword').text('Add').removeAttr('disabled');
                }
            });
        }

        function deleteKeyword(id) {
            $.ajax({
                url: '{{ route('keyword.api.delete') }}',
                type: 'POST',
                data: {
                    _token: "{{ csrf_token() }}",
                    id: id
                },
                success: function(response) {
                    if (response.hasOwnProperty('message')) {
                        $('tr[data-id=' + id + ']').remove();
                        return true;
                    }

                    return false;
                }
            });
        }

        function clearAndCloseModal() {
            $('.keywords-input').tagsinput('removeAll');
            $('#new-keyword .alert').remove();
            $('#new-keyword').modal('toggle');
        }

        function showKeyword(response) {
            $('.keyword-list tbody').empty();
            if (response.hasOwnProperty('error')) {
                $('.keyword-list tbody').append('<tr><td>' + response.error + '</td></tr>');
            } else {
                for (let i = 0; i < response.keywords.length; i++) {
                    let keyword = response.keywords[i];
                    $('.keyword-list tbody').append('<tr data-id="' + keyword.id + '"><td>' + keyword.keyword + '</td><td>' + keyword.lang + '</td><td><span class="check-rank" id="' + keyword.id + '" onClick="getRankKeyword(' + keyword.id + ')"><i class="fa fa-refresh"></i> <span class="rank-number">' + keyword.rank + '</span></span></td><td>Chart</td> <td><button class="btn btn-success btn-sm promote" onClick="promoteKeyword(' + keyword.id + ')" data-id="' + keyword.id + '"><i class="fa fa-signal"></i> Promote</button> <button class="btn btn-danger btn-sm delete" data-id="' + keyword.id + '" onclick="deleteKeyword(' + keyword.id + ')"><i class="fa fa-trash"></i> Delete</button></td></tr>');
                }
            }
        }

        function getRankKeyword(id) {
            $.ajax({
                url: '{{ route('keyword.api.rank') }}',
                type: 'POST',
                data: {
                    _token: "{{ csrf_token() }}",
                    id: id,
                },
                beforeSend: function() {
                    $('span#' + id + ' i').addClass('fa-spin');
                },
                success: function(response) {
                    $('span#' + id + ' span.rank-number').text(response);
                    $('span#' + id + ' i').removeClass('fa-spin');
                }
            });
        }

        function promoteKeyword(idkeyword) {
            let idapp = $('.app').val();
            let country = $('.country').val();
            let href = new_order_url + '/?idapp=' + idapp + '&country=' + country + '&idkeyword=' + idkeyword;
            window.location.href = href;
        }
    </script>
@endsection