@extends('adminlte::page')

@section('title', 'Affiliate Program')

@section('content_header')
    <h1>Affiliate Program</h1>
    <h3>Invite users and earn money for promoting your apps or get payout</h3>
@stop

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Afiliates</h3>
                </div>

                <div class="box-body">
                    <div class="callout callout-info">
                        <h4>Share with other developers and friends the link below</h4>

                        <p>AppRan shares <b>20%</b> of the profit derived from affiliates with partners (only for paypal payments in USD). Revenue share is not limited in time. Affiliate earnings will be applied to your account balance.
                            You can spend money for your own promotion or you can get <b>payout</b> via paypal (starts from $50). </p>
                    </div>

                    <div class="callout callout-info">
                        <a href="">{{ route('register') . '?ref=' . Auth::user()->id }}</a>
                        <br>
                        <h4><strong>{{ $referred }}</strong> users already registered by your link.</h4>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Earned History</h3>
                </div>

                <div class="box-body">
                    <table class="table">
                        <thead>
                            <th>Detail</th>
                            <th>Amount</th>
                            <th>Type</th>
                            <th>Created</th>
                        </thead>

                        <tbody>
                            @foreach($transactions as $transaction)
                                <tr>
                                    <td>{{ $transaction->payment_id }}</td>
                                    <td>${{ $transaction->payment_amount }}</td>
                                    <td><span class="label label-sm label-primary">{{ $transaction->payment_type }}</span></td>
                                    <td>{{ $transaction->created_at->format('M d, Y') }}</td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('js')
    <script>
        $(document).ready(function(){
            $('[data-toggle="tooltip"]').tooltip();
        });
    </script>
@endsection