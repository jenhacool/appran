<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Keyapp;
use App\App;
use Auth;

class AppController extends Controller
{
    public function showAllApp()
    {
        $apps = Keyapp::listApp();

        usort($apps, array($this, 'sortApp'));

        if(Auth::user()->type == 'admin') {
            return view('app.all', compact('apps'));
        }

        $idapps = App::where('user_id', Auth::user()->id)->get()->pluck('app_id')->toArray();

        $apps = array_filter($apps, function($value) use ($idapps) {
            return in_array($value->id, $idapps);
        });

        usort($apps, array($this, 'sortApp'));

        return view('app.all', compact('apps'));
    }

    public function storeNewApp(Request $request)
    {
        $app = [
            'package' => $this->getPackageName($request->url),
            'country' => $request->country
        ];

        $add = Keyapp::storeApp($app);

        $response = json_decode($add);

        if(isset($response->Error)) {
            return redirect(route('app.list'))->with('error', $response->Error);
        }

        $newApp = App::create([
            'app_id' => $response->id,
            'user_id' => Auth::id(),
            'package' => $response->package,
            'developer' => $response->developer,
            'icon' => $response->icon,
            'downloads' => $response->downloads,
            'alive' => true,
            'country' => $response->country
        ]);

        if(!$newApp) {
            return redirect(route('app.list'))->with('error', 'Something wrong happen. Please try again');
        }

        return redirect(route('app.list'));
    }

    public function deleteApp($id)
    {
        $delete = Keyapp::deleteApp($id);

        $response = json_decode($delete);

        if (isset($response->Error)) {
            return redirect(route('app.list'))->with('error', $response->Error);
        }

        App::where('app_id', $id)->delete();

        return redirect(route('app.list'));
    }

    public function getPackageName($url)
    {
        $arr = parse_url($url);
        $new = explode("&",$arr['query']);
        $new1 = explode("=",$new[0]);
        $package = $new1[1];

        return $package;
    }

    public static function sortApp($a, $b) {
        return ($a->id < $b->id) ? 1 : -1;
    }
}
