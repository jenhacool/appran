<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::post('/ajax/login', 'AjaxController@login')->name('ajax.login');
Route::post('/ajax/register', 'AjaxController@register')->name('ajax.register');

Route::prefix('apps')->middleware('auth')->group(function() {
    Route::get('/', 'AppController@showAllApp')->name('app.list');
    Route::post('/', 'AppController@storeNewApp')->name('app.store');
    Route::post('/delete/{id}', 'AppController@deleteApp')->name('app.delete');
});

Route::prefix('keywords')->middleware('auth')->group(function() {
    Route::get('/', 'KeywordController@index')->name('keyword.index');
});

Route::prefix('orders')->middleware('auth')->group(function() {
    Route::get('/', 'OrderController@index')->name('order.index');
    Route::get('/orders/{id}/pause', 'OrderController@pauseOrder')->name('order.pause');
    Route::get('/orders/{id}/cancel', 'OrderController@cancelOrder')->name('order.cancel');
    Route::get('/orders/{id}/resume', 'OrderController@resumeOrder')->name('order.resume');
    Route::get('/list', 'OrderController@listOrder')->name('order.list');
    Route::get('/new', 'OrderController@newOrder')->name('order.new');
    Route::post('/', 'OrderController@storeOrder')->name('order.store');
});

Route::prefix('balances')->middleware('auth')->group(function() {
    Route::get('/', 'PaymentController@index')->name('balance.index');
    Route::post('/compare', 'PaymentController@compare')->name('balance.compare');
    Route::post('/', 'PaymentController@addBalance')->name('balance.add');
    Route::get('/callback', 'PaymentController@callbackBalance')->name('balance.callback');
    Route::post('/transfer', 'PaymentController@transferMoney')->name('balance.transfer');
});

Route::get('/transactions', 'PaymentController@showTransaction')->middleware('auth');

Route::get('/affiliates', 'HomeController@showAffiliatePage')->middleware('auth');

Route::get('/contact', 'HomeController@showContactForm')->middleware('auth');
Route::get('/contact/list', 'HomeController@showContactList')->middleware('auth', 'admin');
Route::get('/contact/mark/{id}', 'HomeController@markContact')->middleware('auth', 'admin')->name('contact.mark');
Route::get('/contact/delete/{id}', 'HomeController@deleteContact')->middleware('auth', 'admin')->name('contact.delete');
Route::post('/contact', 'HomeController@storeContact')->middleware('auth')->name('contact.store');

Route::get('/password', 'HomeController@showUserEditForm')->middleware('auth')->name('user.password');
Route::post('/password', 'HomeController@changeUserPassword')->middleware('auth')->name('user.changepassword');

Route::get('/email', 'HomeController@changeEmailForm')->middleware('auth')->name('user.email');
Route::post('/email', 'HomeController@changeEmailUser')->middleware('auth')->name('user.changeemail');
