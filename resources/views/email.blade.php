@extends('adminlte::page')

@section('title', 'Change Email')

@section('content_header')
    <h1>Change Email</h1>
@stop

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="box">
                <div class="box-header with-border">
                    <h3 class="box-title">Change Email</h3>
                </div>

                <form action="{{ route('user.email') }}" method="POST">
                    @csrf
                    <div class="box-body">
                        @if (session('error'))
                            <div class="alert alert-danger">
                                {{ session('error') }}
                            </div>
                        @endif

                        @if (session('success'))
                            <div class="alert alert-success">
                                {{ session('success') }}
                            </div>
                        @endif

                        <div class="form-group">
                            <label for="">Email</label>
                            <input type="text" name="email" class="form-control" value="{{ Auth::user()->email }}">
                        </div>
                    </div>

                    <div class="box-footer">
                        <button class="btn btn-primary" type="submit">Change Email</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection