@extends('adminlte::page')

@section('title', 'Contact us')

@section('content_header')
    <h1>Contact us</h1>
    <h3>Please, let us know if you find a bug or mistake. Also If you have business inquiries or other questions. Thank you.</h3>
@stop

@section('content')
    <div class="row">
        <div class="col-sm-8">
            <div class="box box-info">
                <div class="box-header with-border">
                    <h3 class="box-title">Contact us</h3>
                </div>
                <form class="form-horizontal" action="{{ route('contact.store') }}" method="POST">
                    @csrf
                    <div class="box-body">
                        <div class="form-group">
                            <label for="inputEmail3" class="col-sm-2 control-label">Email</label>

                            <div class="col-sm-10">
                                <input type="email" class="form-control" id="email" name="email" placeholder="Email" value="">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="inputPassword3" class="col-sm-2 control-label">Message </label>

                            <div class="col-sm-10">
                                <textarea class="form-control" placeholder="Your Message" id="message" name="message"></textarea>
                            </div>
                        </div>

                    </div>
                    <!-- /.box-body -->
                    <div class="box-footer">
                        <button type="submit" class="btn btn-info pull-right">Send</button><br><br>
                        @if(session('success'))
                        <div class="alert alert-success alert-dismissible">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                            <h4><i class="icon fa fa-check"></i>Success!</h4>
                            {{ session('success') }}
                        </div>
                        @endif
                    </div>
                    <!-- /.box-footer -->
                </form>
            </div>
        </div>
        <div class="col-md-4">
            <div class="box box-info">
                <div class="box-header with-border">
                    <h3 class="box-title">Contact Info</h3>
                </div>
                <!-- /.box-header -->
                <!-- form start -->
                <div class="box-body">
                    <p>
                    </p><h4 class="text-left">Office 2501 , Mazaya Business Avenue . Jumeriah, Lake Towers, Dubai - UAE</h4>

                    <h5 class="text-left"><mark>E : care@appran.com</mark></h5>
                    <hr>
                    <p></p>


                </div>
                <!-- /.box-body -->
            </div>
        </div>
    </div>
@endsection

@section('js')
    <script>
        $(document).ready(function(){
            $('[data-toggle="tooltip"]').tooltip();
        });
    </script>
@endsection