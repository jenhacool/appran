@extends('adminlte::page')

@section('title', 'Add New Order')

@section('content_header')
    <h1>Add New Order</h1>
@stop

@section('content')
    @if(session('store_order_error'))
        <div class="alert alert-danger alert-dismissible">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <h4><i class="icon fa fa-ban"></i> Error</h4>
            {{ session('store_order_error') }}
        </div>
    @endif

    @if(session('not_enough_money'))
        <div class="alert alert-danger alert-dismissible">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <h4><i class="icon fa fa-ban"></i> Error</h4>
            {{ session('not_enough_money') }}
        </div>
    @endif

    <div class="row">
        <div class="col-md-12">
            <div class="box">
                <form class="form-horizontal" id="new-order" method="POST" action="{{ route('order.store') }}">
                    @csrf
                    <div class="box-body">
                        <div class="form-group">
                            <label for="" class="col-sm-2 control-label">Country</label>

                            <div class="col-sm-10 form-group-right">
                                <select class="form-control country" name="country">
                                    <option value="US" @if(isset($country) && $country == 'US') selected @endif default>United States</option>
                                    <option value="GB" @if(isset($country) && $country == 'GB') selected @endif>Great Britain</option>
                                    <option value="DE" @if(isset($country) && $country == 'DE') selected @endif>Germany</option>
                                    <option value="IN" @if(isset($country) && $country == 'IN') selected @endif>India</option>
                                    <option value="CA" @if(isset($country) && $country == 'CA') selected @endif>Canada</option>
                                </select>
                                <span>choose one of your apps or <a href="{{ route('app.list') }}">add new here</a></span>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="" class="col-sm-2 control-label">Application</label>

                            <div class="col-sm-10 form-group-right">
                                <select name="idapp" id="app" class="form-control">
                                    @if(!isset($idapp))
                                        <option value="" selected disabled="">Select App</option>
                                    @endif
                                    @foreach($apps as $app)
                                        <option data-icon="{{ $app->icon }}" value="{{ $app->id }}" @if(isset($idapp) && $idapp == $app->id) selected="selected" @endif>{{ $app->title }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="" class="col-sm-2 control-label">Installs</label>

                            <div class="col-sm-10 form-group-right install">
                                <input type="number" name="installs" class="mg-right-20">
                                <span class="mg-right-5"> Split installs for</span>
                                <input class="mg-right-5" type="number" name="days">
                                <span class="mg-right-20">day(s)</span>
                                <span class="mg-right-5">Daily cap</span>
                                <input type="number" name="daily" readonly="readonly">
                                <span class="label label-primary install-price price-calculation" style="display: none"></span>
                                <br>
                                <span>you can set daily cap for your order, installs will be delivered in several days, but limited to daily cap value per day</span>
                                <br>
                                <div class="install-error text-red"></div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="" class="col-sm-2 control-label">Keyword or package</label>

                            <div class="col-sm-10 form-group-right">
                                <select name="idkey" id="keyword" class="mg-right-20">
                                    <option value="package">Package</option>
                                    @if(isset($keywords)))
                                        @foreach($keywords as $keyword)
                                            <option value="{{ $keyword->id }}" @if(isset($idkeyword) && $idkeyword == $keyword->id) selected @endif>{{ $keyword->keyword }}</option>
                                        @endforeach
                                    @endif
                                </select>
                                @if(isset($idkeyword))
                                    <a href="{{ route('keyword.index') . '?idapp=' . $idapp . '&country=' . $country }}" class="add_new_keyword">Add new keyword</a>
                                @endif
                                <br>
                                <span>pick keyword or add new</span>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="" class="col-sm-2 control-label">Rates</label>

                            <div class="col-sm-10 form-group-right">
                                <input type="number" name="rates" class="rates mg-right-20">
                                <select name="rates_type" id="" class="mg-right-20">
                                    <option value="1">5* postitive rates</option>
                                    <option value="2" selected="">5* + 4* positive rates (80%+20%)</option>
                                    <option value="4">1* negative rates</option>
                                    <option value="3">1* + 2* negative rates (80%+20%)</option>
                                </select>
                                <a href="javascript:;" class="label label-danger" onclick="$('.rate-term').toggle();">Terms of Use</a>
                                <span class="label label-primary rate-price price-calculation" style="display: none"></span>
                                <br>
                                <span>add rates to your app</span>
                                <div class="alert alert-warning alert-dismissible rate-term" style="display: none">
                                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                    <h4>Terms Of Use</h4>
                                    <p>Warning! Almost all rates and reviews are getting deleted by Google, we will NOT refund money for rates and reviews. Use them at your own risk.</p>
                                    <p>All rates and reviews will appear on your app's market page in 2-3 hrs after order is completed. After 2 days most of them will be removed and it highly depends on the kind of your app, its organic traffic and usual daily rates increase, so by continuing with rates and reviews order you agree to take this risk.</p>
                                    <p>For less removal try to follow these rules:</p>
                                    <ul>
                                        <li>Do not add tons of rates and reviews for <b>new</b> apps or to apps with a small number of installs. </li>
                                        <li>Do not order more than 10-15% of total number of installs in rates, use 5*+4* stars selector.</li>
                                        <li>Do not order more than 30% of total number of rates in reviews.</li>
                                    </ul>
                                    <p>Rates behavior should look like real user flow, proportionally to your app's organic traffic in the selected country and similar to your current number of rates. Google will remove some of rates or reviews if they find them suspicious.</p>
                                    <p>Example #1: 100 installs + 15 rates + 5 reviews is good.</p>
                                    <p>Example #2: 200 installs + 200 rates to new app or app without rates is bad</p>
                                    <div class="checkbox text-center">
                                        <label>
                                            <input type="checkbox" name="rate-agree" class="rate-agree"> I've read and agree to the refund Terms
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="" class="col-sm-2 control-label">Installs delivery type</label>

                            <div class="col-sm-10 form-group-right">
                                <select name="delivery_type" id="">
                                    <option value="0" selected="">Spread installs within 24h</option>
                                    <option value="1">All installs at once</option>
                                </select>
                                <br>
                                <span>for low traffic apps it's better to spread installs within 24h (similar portion per hour), it's more like an organic traffic</span>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="" class="col-sm-2 control-label"><strong>Price</strong></label>

                            <div class="col-sm-10 form-group-right">
                                <span>Order price is $<span class="total-price"></span></span>
                                <input type="hidden" name="total_price">
                                <br>
                                <span>this sum will be deducted from your balance, check your pricing plan here</span>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="" class="col-sm-2 control-label">Promotional tips</label>

                            <div class="col-sm-10 form-group-right">
                                <span>Please, DON'T change your app's icon, name or publisher name while running an order.</span>
                                <br>
                                <span>Our users will search for your app using keywords, and the only way for them to find it, is by looking at the icon and comparing application name</span>                            </div>
                        </div>
                    </div>
                    <div class="box-footer">
                        <button class="btn btn-success" type="submit"><i class="fa fa-check" aria-hidden="true"></i> Start Promotion</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@stop
@section('css')
    <link rel="stylesheet" href="{{ asset('css/select2.min.css') }}">
    <link rel="stylesheet" href="{{ asset('css/flags.min.css') }}">

    <style>
        .box-body {
            padding-bottom: 0;
        }

        label.error {
            color: #dd4b39 !important;
        }

        .install input {
            width: 60px;
        }

        .app-select-option {
            font-size: 18px;
            line-height: 30px;
            display: block;
        }

        .app-select-option img {
            height: 30px;
            margin-right: 10px;
            float: left;
        }

        .select2 {
            width: 100%;
        }

        .select2-selection {
            height: 34px!important;
        }

        input, select {
            padding: 5px 6px !important;
            line-height: 20px;
            font-size: 14px;
            color: #333333;
            display: inline-block;
            border: 1px solid #e5e5e5;
        }

        .form-group {
            margin-bottom: 0;
            border-bottom: 1px solid #eee;
        }

        .form-group label {
            padding-top: 17px!important;
        }

        .form-group .form-group-right {
            padding: 15px;
            border-left: 1px solid #eee;
        }

        .price-calculation {
            float: right;
            font-size: 100%;
            font-weight: normal;
        }

        .mg-right-20 {
            margin-right: 20px!important;
        }

        .mg-right-5 {
            margin-right: 5px!important;
        }

        .total-price {
            font-weight: 700;
        }

        .rate-term {
            margin: 10px 0 0 0;
        }
    </style>
@endsection

@section('js')
    <script src="{{ asset('js/select2.min.js') }}"></script>
    <script src="{{ asset('js/jquery.validate.min.js') }}"></script>

    <script>
        const new_keyword_url = '{{ route('keyword.index') }}';
        $(document).ready(function() {
            $('[data-toggle="popover"]').popover();

            $('.rate-agree').change(function() {
                if(this.checked) {
                    $('.rate-term').hide();
                } else {
                    $('.rate-term').show();
                }
            });

            $('#app').select2({
                templateResult: format
            });

            $('.install input').change(function() {
                let total = parseInt($('input[name=installs]').val());
                let days = parseInt($('input[name=days]').val());
                let daily = 0;

                if(total && total > 0) {
                    if(total >= days) {
                        daily = Math.ceil(total / days);
                    } else {
                        daily = 1;
                    }
                } else {
                    daily = 0;
                }

                if(days > 0) {
                    $('input[name=daily]').val(daily);
                }

                if(total > 0) {
                    $('.install-price').show();
                    installPrice();
                    updateTotalPrice();
                } else {
                    $('.install-price').hide();
                    $('.rate-price').val().hide();
                }
            });

            $('#app').change(function() {
                let country = $('.country').val();
                let app = $('#app').val();

                let add_new_url = new_keyword_url + '?idapp=' + app + '&country=' + country;
                $('.add_new_keyword').remove();
                $('select#keyword').after('<a href="' + add_new_url + '" class="add_new_keyword">Add new keyword</a>');

                $.ajax({
                    url: '{{ route('keyword.api.list') }}' + '?idapp=' + app + '&country=' + country,
                    type: 'GET',
                    success: function(response) {
                        $('#keyword').empty();
                        $('#keyword').append('<option value="package">Package</option>');
                        if(response.keywords.length > 0) {
                            for(let i = 0;i < response.keywords.length;i++) {
                                let keyword = response.keywords[i];
                                $('#keyword').append('<option value="' + keyword.id + '">' + keyword.keyword + '</option>');
                            }
                        }
                    }
                });
            });

            $('#keyword').change(function() {
                let total = $('input[name=installs]').val();
                if(total > 0) {
                    $('.install-price').show();
                    installPrice();
                    updateTotalPrice();
                } else {
                    $('.install-price').hide();
                    $('.rate-price').hide();
                }
            });

            $('.rates').change(function() {
                let rates = $(this).val();

                if(rates != 0 && $('.rate-agree').prop('checked') != true) {
                    $('.rate-term').show();
                } else {
                    $('.rate-term').hide();
                }

                if($('input[name=installs]').val() > 0) {
                    let rates_price = parseFloat(rates * 0.078).toFixed(2);
                    $('.rate-price').text(rates + ' * 0.078 = ' + rates_price + '$').show();
                    updateTotalPrice();
                }
            });

            $('#new-order').validate({
                rules: {
                    idapp: {
                        required: true
                    },
                    installs: {
                        required: true,
                        min:50
                    },
                    daily: {
                        required: true,
                        min: 50
                    }
                },
                messages: {
                    idapp: {
                        required: 'You must select application'
                    },
                    installs: {
                        required: 'You must set number of installs',
                        min: 'Installs can\'t be less then 50 pcs.'
                    },
                    daily: {
                        required: 'You must set number of days',
                        min: 'Daily cap installs can\'t be less then 50 pcs.'
                    }
                },
                errorPlacement: function(error, element) {
                    error.appendTo(element.parent());
                },
            });
        });

        function format(option) {
            if(!option.id) {
                return option.text;
            }
            var object = $('<div class="app-select-option"><img src="' + $(option.element).attr('data-icon') + '">' + option.text + '</div>');
            return object;
        }

        function installPrice() {
            let install = $('input[name=installs]').val();
            let keyword = $('#keyword').val();
            let price = 0.078;

            if(keyword != 'package') {
                price += 0.065;
            }

            let total = parseFloat(install * price).toFixed(2);

            $('.install-price').text(install + ' * ' + price + ' = ' + total + '$');
        }

        function updateTotalPrice() {
            let install = $('input[name=installs]').val();
            let keyword = $('#keyword').val();
            let price = 0.078;

            if(keyword != 'package') {
                price += 0.065;
            }

            let install_price = parseFloat(install * price);
            let total = install_price;

            let rates = $('.rates').val();

            if($('input[name=installs]').val() > 0 && rates > 0) {
                let rates_price = parseFloat(rates * 0.078);
                total += rates_price;
            }

            $('.total-price').text(total.toFixed(2));
            $('input[name=total_price]').val(total.toFixed(2));
        }
    </script>
@endsection