@php
    $status_badges = [
        '100' => '<span class="label label-success">ACTIVE</span>',
        '120' => '<span class="label label-danger">ACTIVE</span>',
        '250' => '<span class="label label-primary">COMPLETED</span>',
        '150' => '<span class="label label-warning">CANCELED</span>',
        '280' => '<span class="label label-warning">ARCHIVED</span>'
    ];
@endphp
<div class="table-responsive">
    <table id="order-list" class="table table-bordered table-hover">
        <thead>
            <tr>
                <th>#</th>
                <th></th>
                <th>Application</th>
                <th>Keyword</th>
                <th>Country</th>
                <th>Rank</th>
                <th>Installs</th>
                <th>Rates</th>
                <th>Reviews</th>
                <th>Days</th>
                <th>Price</th>
                <th>Start time</th>
                <th>Progress</th>
                <th>Status</th>
                <th>Email</th>
                <th>Action</th>
            </tr>
        </thead>
        <tbody>
            @foreach($orders as $order)
            <tr>
                <td>{{ $order->id }}</td>
                <td>
                    <img width="26px" src="{{ $order->app->icon }}">
                </td>
                <td>
                    <a href="{{ 'https://play.google.com/store/apps/details?id=' . $order->app->package . '&gl=' . $order->app->country }}">{{ $order->app->title }}</a>;
                </td>
                <td>
                    @if($order->details[0]->keyword == null)
                        <span class="label label-success">Package</span>
                    @else
                        <span class="label label-primary">{{ $order->details[0]->keyword }}</span>
                    @endif
                </td>
                <td>
                    <span><i class="flag flag-{{ strtolower($order->country) }}"></i>{{ $order->country }}</span>
                </td>
                <td>
                    @if($order->details[0]->rank != null)
                        <span onClick="getRankKeyword({{ $order->details[0]->idkey }})" class="check-rank" style="cursor: pointer" data-id="{{ $order->details[0]->idkey }}"><i class="fa fa-refresh"></i><span class="rank-number">{{ $order->details[0]->rank }}</span></span>
                    @endif
                </td>
                <td>
                    @if($order->installs > 1)
                        <span class="text-green">{{ $order->installs_actual }}</span> / {{ $order->installs }}
                    @else
                        0
                    @endif
                </td>
                <td>
                    @if($order->rates > 1)
                        <span class="text-green">{{ $order->rates_actual }}</span> / {{ $order->rates }}
                    @else
                        0
                    @endif
                </td>
                <td>
                    @if($order->reviews > 1)
                        <span class="text-green">{{ $order->reviews_actual }}</span> / {{ $order->reviews }}
                    @else
                        0
                    @endif
                </td>
                <td>
                    @if($order->days > 1)
                        <span class="text-green">{{ $order->day }}</span> / {{ $order->days }}
                    @else
                        {{ $order->days }}
                    @endif
                </td>
                <td>
                    @php
                        $install = $order->installs;
                        $rates = $order->rates;
                        $keyword = $order->details[0]->idkey;
                        $price = 0.06;

                        if($keyword != 0) {
                            $price += 0.05;
                        }

                        $total = ($install * $price) + ($rates * 0.06);

                        echo '$' . round($total, 2);   
                    @endphp
                </td>
                <td>{{ $order->time_start }}</td>
                <td>
                    @if(($order->installs_actual / $order->installs) > 1)
                        100%
                    @else
                        {{ round($order->installs_actual / $order->installs * 100)}}
                    @endif
                </td>
                <td>{!! $status_badges[$order->details[0]->status] !!}</td>
                <td>{{ $order->email }}</td>
                <td>
                    @if($order->details[0]->status == 100)
                        <a class="btn btn-default btn-sm pause-button" href="{{ route('order.list') . $order->id }}/pause">
                            <i class="fa fa-pause" aria-hidden="true"></i> Pause
                        </a> 
                        <a class="btn btn-default btn-sm cancel-button" href="{{ route('order.list') . $order->id }}/cancel">
                            <i class="fa fa-trash" aria-hidden="true"></i> Cancel
                        </a>
                    @endif

                    @if($order->details[0]->status == 110)
                        <a class="btn btn-default btn-sm resume-button" href="{{ route('order.list') . $order->id }}/resume">
                            <i class="fa fa-play" aria-hidden="true"></i> Resume
                        </a>
                    @endif
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>
</div>