@extends('adminlte::page')

@section('title', 'Contact Request List')

@section('content_header')
    <h1>Contact Request List</h1>
@stop

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Transaction Info</h3>
                </div>

                <div class="box-body">
                    <table class="table">
                        <thead>
                        <th>Email</th>
                        <th>Message</th>
                        <th>Status</th>
                        <th>Created</th>
                        <th>Action</th>
                        </thead>

                        <tbody>
                        @foreach($contacts as $contact)
                            <tr>
                                <td>{{ $contact->email }}</td>
                                <td>{{ $contact->message }}</td>
                                <td>{{ ($contact->status) ? 'Answered' : 'Not Answer' }}</td>
                                <td>{{ $contact->created_at->format('M d, Y') }}</td>
                                <td>
                                    @if($contact->status == 0)
                                        <a href="{{ route('contact.mark', ['id' => $contact->id]) }}" class="btn btn-success">Mark as Answered</a>
                                    @endif
                                        <a href="{{ route('contact.delete', ['id' => $contact->id]) }}" class="btn btn-danger">Delete</a>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection