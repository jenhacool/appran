<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\User;

class App extends Model
{
    protected $fillable = ['app_id', 'user_id', 'package', 'developer', 'icon', 'downloads', 'alive', 'country'];

    public function user()
    {
    	return $this->belongsTo(User::class);
    }
}
