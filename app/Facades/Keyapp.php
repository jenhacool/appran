<?php

namespace App\Facades;

use \Illuminate\Support\Facades\Facade;

class Keyapp extends Facade
{
    protected static function getFacadeAccessor() {
        return 'keyapp';
    }
}