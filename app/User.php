<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'referred_by'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function idapps()
    {
        return $this->hasMany('App\App', 'user_id', 'id')->select('');
    }

    public function apps()
    {
        return $this->hasMany('App\App','user_id', 'id');
    }

    public function wallet()
    {
        return $this->hasOne('App\Wallet', 'user_id', 'id');
    }

    public function transactions()
    {
        return $this->hasMany('App\Payment', 'user_id')->orderBy('created_at', 'desc');
    }
}
