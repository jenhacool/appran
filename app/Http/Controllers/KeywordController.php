<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\App;
use Keyapp;

class KeywordController extends Controller
{
    public function index(Request $request)
    {
        $apps = Keyapp::listApp();

        if(Auth::user()->type != 'admin') {
            $idapps = App::where('user_id', Auth::user()->id)->get()->pluck('app_id')->toArray();

            $apps = array_filter($apps, function($value) use ($idapps) {
                return in_array($value->id, $idapps);
            });
        }

        $idapp = ($request->get('idapp') != '') ? $request->get('idapp') : '';
        $country = ($request->get('country') != '') ? $request->get('country') : 'US';

        if($idapp == ''){
            return view('keyword.index', compact('apps'));
        }

        $response = Keyapp::getKeywords($idapp, $country);

        if(isset($response->Error)) {
            $error = $response->Error;
            return view('keyword.index', compact('idapp', 'country', 'apps', 'error'));
        }

        $keywords = $response->keywords;

        foreach($keywords as $keyword) {
            $keyword->lang = $this->setCountryData($keyword->lang);
        }

        return view('keyword.index', compact('keywords', 'idapp', 'country', 'apps'));
    }

    public function setCountryData($lang)
    {
        $country = explode('-', $lang)[1];

        $html = '<i class="flag flag-' . strtolower($country) . '"></i> ' . $country;

        return $html;
    }
}
