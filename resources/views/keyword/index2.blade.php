@extends('adminlte::page')

@section('title', 'My Keywords')

@section('content_header')
    <h1>My Keywords</h1>
@stop

@section('content')
    <div class="row" style="margin-bottom: 20px;">
        <div class="col-md-6">
            <form action="" class="form-horizontal">
                <div class="form-group row">
                    <label for="" class="col-sm-4 control-label">Country*</label>
                    <div class="col-md-8">
                        <select class="form-control country" name="country">
                            <option value="" selected disabled="">Select Country</option>
                            <option value="US">United States</option>
                            <option value="GB">Great Britain</option>
                            <option value="DE">Germany</option>
                            <option value="IN">India</option>
                            <option value="CA">Canada</option>
                        </select>
                    </div>
                </div>

                <div class="form-group row">
                    <label for="" class="col-sm-4 control-label">Pick your application*</label>
                    <div class="col-md-8">
                        <select class="form-control app" name="country">
                            <option value="" selected disabled="">Select App</option>
                            @foreach($apps as $app)
                                <option value="{{ $app->id }}">{{ $app->title }}</option>
                            @endforeach
                        </select>
                        <small>If you have no apps you can add new app <a href="{{ route('app.list') }}">here</a></small>
                    </div>
                </div>
                
                <div class="form-group row">
                    <div class="col-md-12">
                        <a href="" class="see-keywords btn btn-success">See Keywords</a>
                    </div>
                </div>
            </form>
        </div>
    </div>
@stop
@section('css')
    <style>
        form label.control-label {
            text-align: left!important;
        }
    </style>
@endsection

@section('js')
    <script>
        $(document).ready(function() {
            $('form.form-horizontal select').change(function() {
                generateLink();
            })
        });

        function generateLink() {
            let country = $('.country').val();
            let app = $('.app').val();
            let link = '{{ route('keyword.index') }}' + '/list?idapp=' + app + '?country=' + country;

            $('.see-keywords').attr('href', link);
        }
    </script>
@endsection