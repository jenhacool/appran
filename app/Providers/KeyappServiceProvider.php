<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Services\Keyapp;

class KeyappServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton('keyapp', function($app) {
            return new Keyapp;
        });
    }

    public function provides()
    {
        return ['keyapp'];
    }
}
