@extends('adminlte::page')

@section('title', 'Appran Dashboard')

@section('content_header')
    <h1>3 Easy Steps to Promote your Awesome App</h1>
@stop

@section('content')
    <div class="row">
        <div class="col-lg-8">
            @if(session('error'))
                <div class="alert alert-danger alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <h4><i class="icon fa fa-ban"></i> Error</h4>
                    {{ session('error') }}
                </div>
            @endif
        </div>

        <div class="col-lg-8">
            <!-- small box -->
            <div class="small-box bg-aqua">
                <div class="inner">
                    <h3>Add your app</h3>
                    <p>Adding new app by inserting application package name or Google Play Store url:</p>
                </div>
                <div class="icon">
                    <i class="fa fa-mobile"></i>
                </div>

            </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-8">
            <!-- small box -->
            <div class="small-box bg-green">
                <div class="inner">
                    <h3>Create order</h3>

                    <p>Press promote button and choose needs options, check the price and press the "Create order and pay" button.</p>
                </div>
                <div class="icon">
                    <i class="fa fa-file-text-o"></i>
                </div>

            </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-8">
            <!-- small box -->
            <div class="small-box bg-yellow">
                <div class="inner">
                    <h3>Just sit back and relax</h3>

                    <p>Your order will start soon. You can track order status on the order list page.</p>
                </div>
                <div class="icon">
                    <i class="fa fa-rocket"></i>
                </div>

            </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-8">
            <!-- small box -->
            <div class="small-box bg-red">
                <div class="inner">
                    <h3>Need help or promotion strategy? Bulk order?</h3>

                    <p>Skype: nextagetech or Email care@appran.com</p>
                </div>
                <div class="icon">
                    <i class="fa fa-envelope-o"></i>
                </div>

            </div>

        </div>
        <!-- ./col -->
    </div>
@stop