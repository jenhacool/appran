<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use Keyapp;
use Input;

class KeywordController extends Controller
{
    public function listKeyword(Request $request)
    {
        $idapp = ($request->get('idapp') != '') ? $request->get('idapp') : '';
        $country = ($request->get('country') != '') ? $request->get('country') : 'US';
        $response = Keyapp::getKeywords($idapp, $country);

        if(isset($response->Error)) {
            return response()->json([
                'error' => $response->Error
            ], 200);
        }

        $keywords = $response->keywords;

        foreach($keywords as $keyword) {
            $keyword->lang = $this->setCountryData($keyword->lang);
        }

        return response()->json([
            'keywords' => $keywords
        ], 200);
    }

    public function setCountryData($lang)
    {
        $country = explode('-', $lang)[1];

        $html = '<i class="flag flag-' . strtolower($country) . '"></i> ' . $country;

        return $html;
    }

    public function saveKeyword(Request $request)
    {
        $response = Keyapp::saveKeywords($request->idapp, $request->country, explode(',', $request->keywords));
        $data = $this->returnData($response);

        return $data;
    }

    public function getRankKeyword(Request $request)
    {
        $id = $request->id;
        $response = Keyapp::getRankKeyword($id);

        if(isset($response->Error)) {
            return response()->json(0, 200);
        }

        return response()->json($response->rank, 200);
    }

    public function deleteKeyword(Request $request)
    {
        $id = $request->id;
        $response = Keyapp::deleteKeyword($id);

        if(isset($response->Error)) {
            return response()->json([
                'error' => $response->Error
            ], 200);
        }

        return response()->json([
            'message' => $response->message
        ], 200);
    }

    public function returnData($response) {
        if(isset($response->Error)) {
            return response()->json([
                'error' => $response->Error
            ], 200);
        }

        return response()->json([
            'keywords' => $response->keywords
        ], 200);
    }
}
